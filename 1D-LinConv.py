# 1-D Linear Convection Equation
# Deepak Ramanath

import numpy as np
import time, sys
import matplotlib.pyplot as plt
from  multiprocessing import Pool

# Function for 1-D convection
# Uses forward in time, backward in space
def ftbs(c,dt,dx,nt,nx,u):
    for n in range(nt):
        uNew = u.copy()
        print uNew
        print "Iteration:", n
        for i in range(1, nx):
            u[i] = uNew[i] - (c * dt / dx) * (u[i] - uNew[i-1]) 

# Problem definition
# *****************
d = 2.0 # domain length
t = 0.625 # total time in seconds
nx = 201 # number of grid points
dx = d / (nx - 1) # delta-x
nt = 100 # Number of iterations
dt = t / nt # Time step
c = 0.5 # Wave speed m/s

# Initial Conditions
# ******************
u = np.ones(nx)
u[int(0.5/dx) : int(1.0/dx + 1)] = 2 # Velocity set to 2 between 0.5 and 1 

# Calling the function
# *******************
ftbs(c,dt,dx,nt,nx,u)

# Grid space for plotting
grid = np.linspace(0,d,nx)
plt.plot(grid, u)
plt.show()
