# 1-D-Linear-Convection-Equation

Solving One-dimensional linear convection equation using Python. The convection equation in 1D is one of the simple and fundamental equations to learn computational fluid dynamics (CFD).